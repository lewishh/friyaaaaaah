from flask import Flask
from flask import Response
import datetime
import time

app = Flask(__name__)
# app.register_blueprint(sse, url_prefix='/streams')


@app.route('/time')
def say_hello_world():
    video = '<html><body><iframe width="420" height="315" src="https://www.youtube.com/watch?v=scNLfr1EP08?' \
            'autoplay=1&loop=1"></iframe></body></html>'

    def event_stream():
        while True:
            yield time_ooooot()
    return Response(event_stream(), mimetype='text/event-stream')


def time_ooooot():
    time.sleep(1)
    polis = datetime.datetime.now()
    return polis


if __name__ == '__main__':
    app.run(debug=True)
